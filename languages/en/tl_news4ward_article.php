<?php

$GLOBALS['TL_LANG']['tl_news4ward_article']['rating_legend'] = 'Rate It-Settings';

$GLOBALS['TL_LANG']['tl_news4ward_article']['addRating'] = array('activate rating', 'Enables the rating for this news article');
$GLOBALS['TL_LANG']['tl_news4ward_article']['rateit_position'] = array('position', 'position of the rating (before or above) the news entry.');

$GLOBALS['TL_LANG']['tl_news4ward_article']['before']          = array('above', 'Display the text above the news entry');
$GLOBALS['TL_LANG']['tl_news4ward_article']['after']           = array('below', 'Display the text below the news entry');
