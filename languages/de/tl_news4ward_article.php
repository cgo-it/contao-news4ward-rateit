<?php

$GLOBALS['TL_LANG']['tl_news4ward_article']['rating_legend'] = 'Rate It-Einstellungen';

$GLOBALS['TL_LANG']['tl_news4ward_article']['addRating'] = array('Rating aktivieren', 'Aktiviert das Rating für diese Nachricht');
$GLOBALS['TL_LANG']['tl_news4ward_article']['rateit_position'] = array('Position', 'Position des Rating (ober- oder unterhalb) des News-Beitrags.');

$GLOBALS['TL_LANG']['tl_news4ward_article']['before']          = array('oberhalb', 'Anzeige des Texts oberhalb des News-Beitrags');
$GLOBALS['TL_LANG']['tl_news4ward_article']['after']           = array('unterhalb', 'Anzeige des Texts unterhalb des News-Beitrags');
