<?php

namespace cgoIT\rateit;

class RateItNews4ward extends RateItFrontend { 
	
	/**
	 * Initialize the controller
	 */
	public function __construct() {
		parent::__construct();
	}

	public function parseArticle($caller, $objArticle, $objTemplate, $arrArticles) {
		if ($objArticle['addRating']) { 
			   $ratingId = $objTemplate->id;
	   		$rating = $this->loadRating($ratingId, 'news4ward');
	   		$stars = !$rating ? 0 : $this->percentToStars($rating['rating']);
	   		$percent = round($rating['rating'], 0)."%";
	   		
	   		$objTemplate->descriptionId = 'rateItRating-'.$ratingId.'-description';
	   		$objTemplate->description = $this->getStarMessage($rating);
	   		$objTemplate->id = 'rateItRating-'.$ratingId.'-news4ward-'.$stars.'_'.$this->intStars;
			   $objTemplate->rateit_class = 'rateItRating';
	   		$objTemplate->itemreviewed = $rating['title'];
	   		$objTemplate->actRating = $this->percentToStars($rating['rating']);
	   		$objTemplate->maxRating = $this->intStars;
	   		$objTemplate->votes = $rating[totalRatings];
	   		
	   		if ($this->strTextPosition == "before") {
	   			$objTemplate->showBefore = true;
	   		}
	   		else if ($this->strTextPosition == "after") {
	   			$objTemplate->showAfter = true;
			}
				
			if ($objArticle['rateit_position'] == 'before') {
				$objTemplate->rateit_rating_before = true;
			} else if ($objArticle['rateit_position'] == 'after') {
				$objTemplate->rateit_rating_after = true;
			}
			
			$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/rateit/public/js/onReadyRateIt.js|static';
			$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/rateit/public/js/rateit.js';
	   		$GLOBALS['TL_CSS'][] = 'system/modules/rateit/public/css/rateit.css||static';
			switch ($GLOBALS['TL_CONFIG']['rating_type']) {
				case 'hearts' :
					$GLOBALS['TL_CSS'][] = 'system/modules/rateit/public/css/heart.css||static';
					break;
				default:
					$GLOBALS['TL_CSS'][] = 'system/modules/rateit/public/css/star.css||static';
			}
		}
	}
}
?>